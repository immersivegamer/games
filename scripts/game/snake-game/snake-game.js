var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var SnakeGame = (function (_super) {
    __extends(SnakeGame, _super);
    function SnakeGame() {
        var _this = _super.apply(this, arguments) || this;
        _this.scale = 10;
        _this.snake = new Snake();
        return _this;
    }
    SnakeGame.prototype.init = function () {
        this.snake.head = createVector(20, 20);
        this.scale = 10;
        this.food = this.newFood();
    };
    SnakeGame.prototype.start = function () {
        this.snake.add();
        this.snake.add();
    };
    SnakeGame.prototype.update = function () {
        background(50);
        this.wrapSnake();
        this.drawFood();
    };
    SnakeGame.prototype.wrapSnake = function () {
        var s = this.size;
        var p = this.snake.head;
        if (p.x >= s.x)
            p.x = 0;
        if (p.x < 0)
            p.x = s.x - this.scale;
        if (p.y >= s.y)
            p.y = 0;
        if (p.y < 0)
            p.y = s.y - this.scale;
        this.snake.head = p;
    };
    SnakeGame.prototype.newFood = function () {
        return createVector(random(0, this.size.x), random(0, this.size.y));
    };
    SnakeGame.prototype.drawFood = function () {
        fill(75);
        drawBlock(this.food);
    };
    return SnakeGame;
}(Game));
function drawBlock(pos) {
    rect(pos.x, pos.y, 10, 10);
}
var game;
function setup() {
    game = new SnakeGame(300, 300);
    game.setup();
}
function draw() {
    game.draw();
}
