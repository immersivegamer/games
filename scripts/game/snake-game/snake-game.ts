class SnakeGame extends Game
{
    scale:number = 10;
    snake:Snake = new Snake();
    food:Vector;

    init()
    {
        //this.snake = new Snake();
        this.snake.head = createVector(20, 20);
        this.scale = 10;

        this.food = this.newFood();
    }

    start()
    {
        this.snake.add();
        this.snake.add();
    }

    update()
    {
        background(50);
        this.wrapSnake();
        this.drawFood();
    }

    wrapSnake()
    {
        var s = this.size;
        var p = this.snake.head;
        if(p.x >= s.x)
            p.x = 0;
        if(p.x < 0)
            p.x = s.x - this.scale;
        if(p.y >= s.y)
            p.y = 0;
        if(p.y < 0)
            p.y = s.y - this.scale;
        this.snake.head = p;
    }

    newFood()
    {
        return createVector(random(0, this.size.x), random(0, this.size.y));
    }

    drawFood()
    {
        fill(75);
        drawBlock(this.food);
    }
}

function drawBlock(pos)
{
    rect(pos.x, pos.y, 10, 10);
}

var game;
function setup()
{
    game = new SnakeGame(300, 300);
    game.setup();
}

function draw()
{
    game.draw();
}
