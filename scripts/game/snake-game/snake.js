var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var DIR;
(function (DIR) {
    DIR[DIR["RIGHT"] = 0] = "RIGHT";
    DIR[DIR["LEFT"] = 1] = "LEFT";
    DIR[DIR["UP"] = 2] = "UP";
    DIR[DIR["DOWN"] = 3] = "DOWN";
})(DIR || (DIR = {}));
var Snake = (function (_super) {
    __extends(Snake, _super);
    function Snake() {
        var _this = _super.apply(this, arguments) || this;
        _this.speed = 10;
        _this.tail = new Array();
        _this.head = createVector(0, 0);
        _this.dir = DIR.RIGHT;
        _this.ticker = 0;
        _this.tick = 15;
        _this.ticked = false;
        return _this;
    }
    Snake.prototype.onStart = function () {
        this.tail[0] = this.head;
    };
    Snake.prototype.onUpdate = function () {
        this.timer();
        this.getDir();
        if (this.ticked) {
            this.calcTail();
            this.move();
        }
    };
    Snake.prototype.onDraw = function () {
        this.drawBody();
    };
    Snake.prototype.getDir = function () {
        if (keyIsDown(P5.INPUT.DOWN_ARROW) && this.dir != DIR.UP) {
            this.dir = DIR.DOWN;
        }
        if (keyIsDown(P5.INPUT.UP_ARROW) && this.dir != DIR.DOWN) {
            this.dir = DIR.UP;
        }
        if (keyIsDown(P5.INPUT.LEFT_ARROW) && this.dir != DIR.RIGHT) {
            this.dir = DIR.LEFT;
        }
        if (keyIsDown(P5.INPUT.RIGHT_ARROW) && this.dir != DIR.LEFT) {
            this.dir = DIR.RIGHT;
        }
    };
    Snake.prototype.timer = function () {
        if (this.ticked)
            this.ticked = false;
        if (this.ticker >= this.tick) {
            this.ticked = true;
            this.ticker = 0;
            return;
        }
        this.ticker += 1;
    };
    Snake.prototype.move = function () {
        if (this.dir == DIR.DOWN) {
            this.head.y += this.speed;
        }
        else if (this.dir == DIR.UP) {
            this.head.y -= this.speed;
        }
        else if (this.dir == DIR.LEFT) {
            this.head.x -= this.speed;
        }
        else if (this.dir == DIR.RIGHT) {
            this.head.x += this.speed;
        }
    };
    Snake.prototype.drawBody = function () {
        fill(100);
        drawBlock(this.head);
        for (var i = 0; i < this.tail.length; i++) {
            drawBlock(this.tail[i]);
        }
    };
    Snake.prototype.add = function () {
        console.log("adding more snake");
        append(this.tail, this.head.copy());
    };
    Snake.prototype.calcTail = function () {
        for (var i = this.tail.length - 1; i > 0; i--) {
            this.tail[i].x = this.tail[i - 1].x;
            this.tail[i].y = this.tail[i - 1].y;
        }
    };
    return Snake;
}(Behaviour));
