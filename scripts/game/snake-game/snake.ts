enum DIR { RIGHT, LEFT, UP, DOWN}
class Snake extends Behaviour
{
    speed:number = 10;
    tail:Array<Vector> = new Array<Vector>();
    head:Vector = createVector(0,0);
    dir:DIR = DIR.RIGHT;

    ticker:number = 0;
    tick:number = 15;
    ticked = false;

    onStart()
    {
        this.tail[0] = this.head;
        //append(this.tail, this.head);
    }

    onUpdate()
    {
        this.timer();

        this.getDir();

        if(this.ticked)
        {
            this.calcTail();
            this.move();
            //console.log("snake: x:"+this.head.x+" y:"+this.head.y);
        }
    }

    onDraw()
    {
        //console.log("snake ticked");
        this.drawBody();
    }

    getDir()
    {
        if(keyIsDown(P5.INPUT.DOWN_ARROW) && this.dir != DIR.UP)
        {
            this.dir = DIR.DOWN;
        }
        if (keyIsDown(P5.INPUT.UP_ARROW) && this.dir != DIR.DOWN) {
            this.dir = DIR.UP;
        }
        if (keyIsDown(P5.INPUT.LEFT_ARROW) && this.dir != DIR.RIGHT) {
            this.dir = DIR.LEFT;
        }
        if (keyIsDown(P5.INPUT.RIGHT_ARROW) && this.dir != DIR.LEFT) {
            this.dir = DIR.RIGHT;
        }
    }

    timer()
    {
        if(this.ticked)
            this.ticked = false;

        if(this.ticker >= this.tick)
        {
            this.ticked = true;
            this.ticker = 0;
            return;
        }

        this.ticker += 1;
    }

    move()
    {
        //console.log("Snake moving");
        if(this.dir == DIR.DOWN)
        {
            this.head.y +=this.speed;
        }
        else if (this.dir == DIR.UP) {
            this.head.y -=this.speed;
        }
        else if (this.dir == DIR.LEFT) {
            this.head.x -=this.speed;
        }
        else if (this.dir == DIR.RIGHT) {
            //console.log("Movning right");
            this.head.x +=this.speed;
        }
    }

    drawBody()
    {
        fill(100);
        drawBlock(this.head);
        for(var i = 0; i < this.tail.length; i++)
        {
            drawBlock(this.tail[i]);
        }
    }

    add()
    {
        console.log("adding more snake")
        append(this.tail, this.head.copy());
    }

    calcTail()
    {
        for(var i = this.tail.length - 1; i > 0; i--)
        {
            this.tail[i].x = this.tail[i-1].x;
            this.tail[i].y = this.tail[i-1].y;
        }
    }
}
