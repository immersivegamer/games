class Game
{
    size:Vector;
    constructor(x, y)
    {
        this.size = createVector(x, y);
    }

    init(){}
    start(){}
    update(){}

    draw ()
    {
        this.update();
        Behaviour.updateAll();
    }

    setup ()
    {
        createCanvas(this.size.x, this.size.y);
        this.init();
        this.draw();
        this.start();
    }
}

class Behaviour
{
    static behaviours:Array<Behaviour> = [];
    static startList:Array<Behaviour> = [];
    static awakeList:Array<Behaviour> = [];
    static nextID:number = 0;
    id:number;
    constructor()
    {
        Behaviour.nextID = Behaviour.nextID + 1;
        this.id = Behaviour.nextID;
        Behaviour.behaviours[this.id] = this;
        Behaviour.awakeList[this.id] = this;
        Behaviour.startList[this.id] = this;
        //this.start();
    }

    static updateAll()
    {
        //console.log("updating b")
        Behaviour.awakeList.forEach(Behaviour.awakeBehaviours);
        Behaviour.startList.forEach(Behaviour.startBehaviours);
        Behaviour.behaviours.forEach(Behaviour.updateBehaviours);
        Behaviour.behaviours.forEach(Behaviour.drawBehaviours);
    }

    static awakeBehaviours(self)
    {
        //console.log("awaking "+self)
        delete Behaviour.awakeList[self.id];
        self.update();
    }

    static startBehaviours(self)
    {
        //console.log("starting "+self)
        delete Behaviour.startList[self.id];
        self.start();
    }

    static updateBehaviours(self)
    {
        self.update();
    }
    static drawBehaviours(self)
    {
        self.draw();
    }

    awake()
    {
        this.onAwake();
    }
    protected onAwake(){}

    start()
    {
        this.onStart();
    }
    protected onStart(){}

    update()
    {
        this.onUpdate();
    }
    protected onUpdate(){}

    draw()
    {
        this.onDraw();
    }
    protected onDraw(){}

    destroy()
    {
        this.onDestroy();
        delete Behaviour.behaviours[this.id];
    }
    protected onDestroy(){}
}

const GAME_COLOR =
{
    GREEN:0x22ba3a,
    YELLOW:0xfbff4a,
    RED:0xb8575d,
    WHITE:0xffffff,
    BLACK:0x000000
};

const P5 =
{
// GRAPHICS RENDERER
GRAPHICS_RENDERER:
{
    P2D: 'p2d',
    WEBGL: 'webgl'
},

// ENVIRONMENT
ENVIRONMENT :
{
    ARROW: 'default',
    CROSS: 'crosshair',
    HAND: 'pointer',
    MOVE: 'move',
    TEXT: 'text',
    WAIT: 'wait'
},

// TRIGONOMETRY
TRIGONOMETRY :
    {
    /**
     * HALF_PI is a mathematical constant with the value
     * 1.57079632679489661923. It is half the ratio of the
     * circumference of a circle to its diameter. It is useful in
     * combination with the trigonometric functions sin() and cos().
     *
     * @property HALF_PI
     *
     * @example
     * <div><code>
     * arc(50, 50, 80, 80, 0, HALF_PI);
     * </code></div>
     *
     * @alt
     * 80x80 white quarter-circle with curve toward bottom right of canvas.
     *
     */
    HALF_PI: 1.57079632679489661923,
    /**
     * PI is a mathematical constant with the value
     * 3.14159265358979323846. It is the ratio of the circumference
     * of a circle to its diameter. It is useful in combination with
     * the trigonometric functions sin() and cos().
     *
     * @property PI
     *
     * @example
     * <div><code>
     * arc(50, 50, 80, 80, 0, PI);
     * </code></div>
     *
     * @alt
     * white half-circle with curve toward bottom of canvas.
     *
     */
    PI: 3.14159265358979323846,
    /**
     * QUARTER_PI is a mathematical constant with the value 0.7853982.
     * It is one quarter the ratio of the circumference of a circle to
     * its diameter. It is useful in combination with the trigonometric
     * functions sin() and cos().
     *
     * @property QUARTER_PI
     *
     * @example
     * <div><code>
     * arc(50, 50, 80, 80, 0, QUARTER_PI);
     * </code></div>
     *
     * @alt
     * white eighth-circle rotated about 40 degrees with curve bottom right canvas.
     *
     */
    QUARTER_PI: 0.7853982,
    /**
     * TAU is an alias for TWO_PI, a mathematical constant with the
     * value 6.28318530717958647693. It is twice the ratio of the
     * circumference of a circle to its diameter. It is useful in
     * combination with the trigonometric functions sin() and cos().
     *
     * @property TAU
     *
     * @example
     * <div><code>
     * arc(50, 50, 80, 80, 0, TAU);
     * </code></div>
     *
     * @alt
     * 80x80 white ellipse shape in center of canvas.
     *
     */
    TAU: 6.28318530717958647693,
    /**
     * TWO_PI is a mathematical constant with the value
     * 6.28318530717958647693. It is twice the ratio of the
     * circumference of a circle to its diameter. It is useful in
     * combination with the trigonometric functions sin() and cos().
     *
     * @property TWO_PI
     *
     * @example
     * <div><code>
     * arc(50, 50, 80, 80, 0, TWO_PI);
     * </code></div>
     *
     * @alt
     * 80x80 white ellipse shape in center of canvas.
     *
     */


    TWO_PI: 6.28318530717958647693,
    DEGREES: 'degrees',
    RADIANS: 'radians'
},

// SHAPE
SHAPE :
{
    CORNER: 'corner',
    CORNERS: 'corners',
    RADIUS: 'radius',
    RIGHT: 'right',
    LEFT: 'left',
    CENTER: 'center',
    TOP: 'top',
    BOTTOM: 'bottom',
    BASELINE: 'alphabetic',
    POINTS: 0x0000,
    LINES: 0x0001,
    LINE_STRIP: 0x0003,
    LINE_LOOP: 0x0002,
    TRIANGLES: 0x0004,
    TRIANGLE_FAN: 0x0006,
    TRIANGLE_STRIP: 0x0005,
    QUADS: 'quads',
    QUAD_STRIP: 'quad_strip',
    CLOSE: 'close',
    OPEN: 'open',
    CHORD: 'chord',
    PIE: 'pie',
    PROJECT: 'square', // PEND: careful this is counterintuitive
    SQUARE: 'butt',
    ROUND: 'round',
    BEVEL: 'bevel',
    MITER: 'miter',
},

// COLOR
COLOR : {
    RGB: 'rgb',
    HSB: 'hsb',
    HSL: 'hsl',
},

// DOM EXTENSION
DOM_EXTENSION :
{
    AUTO: 'auto'
},

// INPUT
INPUT :
{
    ALT: 18,
    BACKSPACE: 8,
    CONTROL: 17,
    DELETE: 46,
    DOWN_ARROW: 40,
    ENTER: 13,
    ESCAPE: 27,
    LEFT_ARROW: 37,
    OPTION: 18,
    RETURN: 13,
    RIGHT_ARROW: 39,
    SHIFT: 16,
    TAB: 9,
    UP_ARROW: 38
},

// RENDERING
RENDERING :
{
    BLEND: 'normal',
    ADD: 'lighter',
    //ADD: 'add', //
    //SUBTRACT: 'subtract', //
    DARKEST: 'darken',
    LIGHTEST: 'lighten',
    DIFFERENCE: 'difference',
    EXCLUSION: 'exclusion',
    MULTIPLY: 'multiply',
    SCREEN: 'screen',
    REPLACE: 'source-over',
    OVERLAY: 'overlay',
    HARD_LIGHT: 'hard-light',
    SOFT_LIGHT: 'soft-light',
    DODGE: 'color-dodge',
    BURN: 'color-burn'
},

// FILTERS
FILTERS :
{
    THRESHOLD: 'threshold',
    GRAY: 'gray',
    OPAQUE: 'opaque',
    INVERT: 'invert',
    POSTERIZE: 'posterize',
    DILATE: 'dilate',
    ERODE: 'erode',
    BLUR: 'blur'
},
// TYPOGRAPHY
TYPOGRAPHY :
{
    NORMAL: 'normal',
    ITALIC: 'italic',
    BOLD: 'bold'
}
}
