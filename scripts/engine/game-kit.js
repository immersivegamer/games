var Game = (function () {
    function Game(x, y) {
        this.size = createVector(x, y);
    }
    Game.prototype.init = function () { };
    Game.prototype.start = function () { };
    Game.prototype.update = function () { };
    Game.prototype.draw = function () {
        this.update();
        Behaviour.updateAll();
    };
    Game.prototype.setup = function () {
        createCanvas(this.size.x, this.size.y);
        this.init();
        this.draw();
        this.start();
    };
    return Game;
}());
var Behaviour = (function () {
    function Behaviour() {
        Behaviour.nextID = Behaviour.nextID + 1;
        this.id = Behaviour.nextID;
        Behaviour.behaviours[this.id] = this;
        Behaviour.awakeList[this.id] = this;
        Behaviour.startList[this.id] = this;
    }
    Behaviour.updateAll = function () {
        Behaviour.awakeList.forEach(Behaviour.awakeBehaviours);
        Behaviour.startList.forEach(Behaviour.startBehaviours);
        Behaviour.behaviours.forEach(Behaviour.updateBehaviours);
        Behaviour.behaviours.forEach(Behaviour.drawBehaviours);
    };
    Behaviour.awakeBehaviours = function (self) {
        delete Behaviour.awakeList[self.id];
        self.update();
    };
    Behaviour.startBehaviours = function (self) {
        delete Behaviour.startList[self.id];
        self.start();
    };
    Behaviour.updateBehaviours = function (self) {
        self.update();
    };
    Behaviour.drawBehaviours = function (self) {
        self.draw();
    };
    Behaviour.prototype.awake = function () {
        this.onAwake();
    };
    Behaviour.prototype.onAwake = function () { };
    Behaviour.prototype.start = function () {
        this.onStart();
    };
    Behaviour.prototype.onStart = function () { };
    Behaviour.prototype.update = function () {
        this.onUpdate();
    };
    Behaviour.prototype.onUpdate = function () { };
    Behaviour.prototype.draw = function () {
        this.onDraw();
    };
    Behaviour.prototype.onDraw = function () { };
    Behaviour.prototype.destroy = function () {
        this.onDestroy();
        delete Behaviour.behaviours[this.id];
    };
    Behaviour.prototype.onDestroy = function () { };
    return Behaviour;
}());
Behaviour.behaviours = [];
Behaviour.startList = [];
Behaviour.awakeList = [];
Behaviour.nextID = 0;
var GAME_COLOR = {
    GREEN: 0x22ba3a,
    YELLOW: 0xfbff4a,
    RED: 0xb8575d,
    WHITE: 0xffffff,
    BLACK: 0x000000
};
var P5 = {
    GRAPHICS_RENDERER: {
        P2D: 'p2d',
        WEBGL: 'webgl'
    },
    ENVIRONMENT: {
        ARROW: 'default',
        CROSS: 'crosshair',
        HAND: 'pointer',
        MOVE: 'move',
        TEXT: 'text',
        WAIT: 'wait'
    },
    TRIGONOMETRY: {
        HALF_PI: 1.57079632679489661923,
        PI: 3.14159265358979323846,
        QUARTER_PI: 0.7853982,
        TAU: 6.28318530717958647693,
        TWO_PI: 6.28318530717958647693,
        DEGREES: 'degrees',
        RADIANS: 'radians'
    },
    SHAPE: {
        CORNER: 'corner',
        CORNERS: 'corners',
        RADIUS: 'radius',
        RIGHT: 'right',
        LEFT: 'left',
        CENTER: 'center',
        TOP: 'top',
        BOTTOM: 'bottom',
        BASELINE: 'alphabetic',
        POINTS: 0x0000,
        LINES: 0x0001,
        LINE_STRIP: 0x0003,
        LINE_LOOP: 0x0002,
        TRIANGLES: 0x0004,
        TRIANGLE_FAN: 0x0006,
        TRIANGLE_STRIP: 0x0005,
        QUADS: 'quads',
        QUAD_STRIP: 'quad_strip',
        CLOSE: 'close',
        OPEN: 'open',
        CHORD: 'chord',
        PIE: 'pie',
        PROJECT: 'square',
        SQUARE: 'butt',
        ROUND: 'round',
        BEVEL: 'bevel',
        MITER: 'miter',
    },
    COLOR: {
        RGB: 'rgb',
        HSB: 'hsb',
        HSL: 'hsl',
    },
    DOM_EXTENSION: {
        AUTO: 'auto'
    },
    INPUT: {
        ALT: 18,
        BACKSPACE: 8,
        CONTROL: 17,
        DELETE: 46,
        DOWN_ARROW: 40,
        ENTER: 13,
        ESCAPE: 27,
        LEFT_ARROW: 37,
        OPTION: 18,
        RETURN: 13,
        RIGHT_ARROW: 39,
        SHIFT: 16,
        TAB: 9,
        UP_ARROW: 38
    },
    RENDERING: {
        BLEND: 'normal',
        ADD: 'lighter',
        DARKEST: 'darken',
        LIGHTEST: 'lighten',
        DIFFERENCE: 'difference',
        EXCLUSION: 'exclusion',
        MULTIPLY: 'multiply',
        SCREEN: 'screen',
        REPLACE: 'source-over',
        OVERLAY: 'overlay',
        HARD_LIGHT: 'hard-light',
        SOFT_LIGHT: 'soft-light',
        DODGE: 'color-dodge',
        BURN: 'color-burn'
    },
    FILTERS: {
        THRESHOLD: 'threshold',
        GRAY: 'gray',
        OPAQUE: 'opaque',
        INVERT: 'invert',
        POSTERIZE: 'posterize',
        DILATE: 'dilate',
        ERODE: 'erode',
        BLUR: 'blur'
    },
    TYPOGRAPHY: {
        NORMAL: 'normal',
        ITALIC: 'italic',
        BOLD: 'bold'
    }
};
